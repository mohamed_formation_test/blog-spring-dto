package com.example.blogspringdto.repository;


import com.example.blogspringdto.entity.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Gulten Ulukal
 */

// @Repository // infact no need to add...
// @Transactional // infact no need to add...

@Repository
public interface CommentRepository extends JpaRepository<Comment,Long> {

    List<Comment> findByPostId(long postId);
}
